*** Settings ***

Documentation   Importing web and robotframework built-in keywords.
Library  SeleniumLibrary   timeout=10
Library    Collections
Library    OperatingSystem
Library    FakerLibrary
Variables         ../../resource/settings/const.py
Resource        ../../resource/settings/page_objects.robot
*** Variables ***
${BROWSER}        chrome
${DELAY}          0.5
${SELENOID_SERVER}    http://localhost:4444/wd/hub

*** Keywords ***
Open Browser to Page
    [Documentation]
    ${desired_caps} =   create dictionary  enableVNC=${True}
    Open Browser    ${PAGE_URL}    ${BROWSER}    None    ${SELENOID_SERVER}    desired_capabilities=${desired_caps}

    Maximize Browser Window
    Set Selenium Speed    ${DELAY}

search googl.com for ${search term}
    [Documentation]
    Wait Until Page Contains Element    ${google search field}
    Input Text    ${google search field}   ${search term}
    Wait Until Page Contains Element    ${google search button}
    Click Element     ${google search button}

write vuule comment
    [Documentation]
    ${comment}=    FakerLibrary.Sentences    nb=3
    Select Frame    v-comments
    Click Element      css=#write-comment-1 > div.ql-editor
    Input Text      css=#write-comment-1 > div.ql-editor    ${comment}
    Input Text      ${vuule username field}    ${VUULE_USERNAME}
    Input Text      ${vuule password field}    ${PASSWORD}
    Click Element     ${vuule checkbox field}
    Click Element     xpath=//span[contains(.,'Sign in')]
    Wait Until Page Contains Element    xpath=//span[contains(.,'Post')]
    Click Element     xpath=//span[contains(.,'Post')]
    Wait Until Page Contains    Your comment is under moderation and will be approved by the site moderator. Thank you for your patience.

